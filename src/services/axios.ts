import axios from "axios";
import router from "../router";
import { useAuthenticationStore } from "../modules/login/dataprovider/authenticationStore";
const instance = axios.create({
  baseURL: "http://localhost:5055/",
});

instance.interceptors.request.use(function (config) {
  if (!useAuthenticationStore().isAuthenticated()) {
    router.push({ name: 'login' });
  }
  return config;
}, function (error) {
  return Promise.reject(error);
});

export default instance;
