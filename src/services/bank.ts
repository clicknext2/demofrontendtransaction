import http from "./axios";
async function getBanks() {
    var result = await http.get("MasterData/GetBanks",{
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Authorization': `Bearer ${localStorage.getItem('userToken')}` 
        }
      })
      return result;
}

export default{
    getBanks,
}