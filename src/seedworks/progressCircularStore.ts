import { defineStore } from "pinia";
import { ref } from "vue";

export const useProgressCircularStore = defineStore("handleProgressCircular", () => {
    const progressCircular = ref(false);

    function progressCircularControl(value:boolean) {
        progressCircular.value = value;
    }

    function circularOn() {
        progressCircular.value = true;
    }

    function circularOff() {
        setTimeout(() => {
            progressCircular.value = false;
          }, 100);
    }
    return {
        progressCircularControl,
        progressCircular,
        circularOn,
        circularOff
    }
});
