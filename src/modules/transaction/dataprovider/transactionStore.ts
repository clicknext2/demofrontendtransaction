import { defineStore } from "pinia";
import { ref } from "vue";
import { useRouter } from "vue-router";
import transactionService from "../../../services/transaction";
import bankService from "../../../services/bank";
import { useProgressCircularStore } from "../../../seedworks/progressCircularStore";

export const useTransactionStore = defineStore("handleTransaction", () => {
    const router = useRouter();
    const accountSelected = ref<AccountResponse | null>(null);
    const accountResults = ref<AccountResponse[]>([]);
    const bankResults = ref<Bank[]>([]);
    const transferPopupToggle = ref(false);
    const depositPopupToggle = ref(false);
    const withdrawPopupToggle = ref(false);
    const tab = ref(0);
    
    function selectAccount(account: AccountResponse) {
        accountSelected.value = account;
        localStorage.setItem('accountNumber', account.accountNumber);
        localStorage.setItem('bankID', account.bankID.toString());
        router.push({ name: 'transaction' });
    }

    async function getTransactions() {
        try {
            useProgressCircularStore().circularOn();
            const userId: string = localStorage.getItem('userId') as string;
            const result = await transactionService.getTransactions(userId);
            accountResults.value = result.data.data

            const accountNumber: string | null = localStorage.getItem('accountNumber') as string;
            accountSelected.value = accountResults.value.find(x => x.accountNumber === accountNumber) ?? null;
            useProgressCircularStore().circularOff();
        } catch (e) {
            useProgressCircularStore().circularOff();
            console.log(e);
        }
    }

    async function getMasterData() {
        try {
            useProgressCircularStore().circularOn();
            const result = await bankService.getBanks();
            bankResults.value = result.data.data;
            useProgressCircularStore().circularOff();
        } catch (e) {
            useProgressCircularStore().circularOff();
            console.log(e);
        }
    }
    return {
        router,
        selectAccount,
        accountSelected,
        accountResults,
        getTransactions,
        transferPopupToggle,
        getMasterData,
        bankResults,
        depositPopupToggle,
        withdrawPopupToggle,
        tab
    }
});
