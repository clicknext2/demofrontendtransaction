export enum TransactionType {
    Deposit = 1,
    Withdraw = 2,
    Transfer = 3,
    Receive = 4
}
