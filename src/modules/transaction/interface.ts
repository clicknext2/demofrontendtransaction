interface Account {
  id: number;
  number: string;
  name: string;
  bankName: string;
  balance: string;
}

interface AccountResponse {
  accountID: string;
  accountName: string;
  accountNumber : string;
  remain: number;
  bankID: number;
  bankName: string;
  transactions: Transaction[];
}

interface Transaction {
  transactionID: string,
  date: Date,
  amount: number,
  fromAccountName: string,
  toAccountName: string,
  transactionType: number
}

interface Bank {
  bankID: number,
  name: string
}

interface TransferRequest {
  receiverBankID: number | null;
  amount: number | null;
  toAccountNumber: string | null;
}