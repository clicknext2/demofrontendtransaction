/**
 * router/index.ts
 *
 * Automatic routes for `./src/pages/*.vue`
 */

// Composables
import { createRouter, createWebHistory } from 'vue-router'
import Index from '../modules/login/Login.vue'
import Transaction from '../modules/transaction/Transaction.vue'
import SelectAccount from '../modules/transaction/SelectAccount.vue'

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: Index
    },
    {
      path: '/transaction',
      name: 'transaction',
      component: Transaction,
      meta: { requiresAuth: true } 
    },
    {
      path: '/selectaccount',
      name: 'selectaccount',
      component: SelectAccount,
      meta: { requiresAuth: true }  
    }
  ]
})

export default router 
